"""Softmax."""

scores = [3.0, 1.0, 0.2]

import numpy as np

def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    e_x = np.exp(x)
    res = e_x / np.sum(e_x, axis=0)
    return res

scores_times_one_tenth = np.array(scores) * 0.1
print("Softmax of scores times one tenth:")
print(softmax(scores_times_one_tenth))

scores_times_ten = np.array(scores) * 10
print("Softmax of scores times ten:")
print(softmax(scores_times_ten))

print("Softmax of scores:")
print(softmax(scores))

# Plot softmax curves
import matplotlib.pyplot as plt
x = np.arange(-2.0, 6.0, 0.1)
scores = np.vstack([x, np.ones_like(x), 0.2 * np.ones_like(x)])

plt.plot(x, softmax(scores).T, linewidth=2)
plt.show()

