import cPickle as pickle
import numpy as np
import tensorflow as tf

image_size = 28
num_labels = 10

pickle_file = '../../1-machine-learning-to-deep-learning/1_notmnist/notMNIST.pickle'

def reformat(dataset, labels):
    """
    Reformat into a shape that's more adapted to the models we're going to
    train:

    - data as a flat matrix
    - labels as float 1-hot encoding
    """

    dataset = dataset.reshape(-1, image_size * image_size).astype(np.float32)
    # Map
    # 0 to [1.0, 0.0, 0.0, ...],
    # 1 to [0.0, 1.0, 0.0, ...], etc
    labels = (np.arange(num_labels) == labels[:, np.newaxis]).astype(np.float32)
    return dataset, labels

with open(pickle_file, 'rb') as f:
    save = pickle.load(f)
    train_dataset = save['train_dataset']
    train_labels = save['train_labels']
    valid_dataset = save['valid_dataset']
    valid_labels = save['valid_labels']
    test_dataset = save['test_dataset']
    test_labels = save['test_labels']

    # hint to gc
    del save

    print 'Training set', train_dataset.shape, train_labels.shape
    print 'Validation set', valid_dataset.shape, valid_labels.shape
    print 'Test set', test_dataset.shape, test_labels.shape

    train_dataset, train_labels = reformat(train_dataset, train_labels)
    valid_dataset, valid_labels = reformat(valid_dataset, valid_labels)
    test_dataset, test_labels = reformat(test_dataset, test_labels)

# prepare graph
batch_size = 128
image_size = 28
image_pixels = image_size * image_size
hidden_size = 1024

graph = tf.Graph()
with graph.as_default():
    # Load the training, validation and test data into constant that are
    # attached to the graph.

    # In the mini-match version we'll use placeholder for training data that
    # will be fed at run time with real data.
    tf_train_dataset = tf.placeholder(tf.float32,
            shape = (batch_size, image_size * image_size))
    tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels))

    tf_valid_dataset = tf.constant(valid_dataset)
    tf_test_dataset = tf.constant(test_dataset)

    with tf.name_scope('hidden'):
        hidden_weights = tf.Variable(tf.truncated_normal([image_pixels, hidden_size]))

        hidden_biases = tf.Variable(tf.zeros([hidden_size]))

        hidden = tf.nn.relu(tf.matmul(tf_train_dataset, hidden_weights) +
                hidden_biases)

    with tf.name_scope('softmax_linear'):
        # Weights are initialized using random values following a truncated normal
        # distribution.
        weights = tf.Variable(tf.truncated_normal([hidden_size, num_labels]))

        # Biases are initialized to zero.
        biases = tf.Variable(tf.zeros([num_labels]))

        logits = tf.matmul(hidden, weights) + biases

    # We compute the softmax and cross-entropy and take average of this
    # cross-entropy across all training examples: that's our loss.
    loss = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(logits, tf_train_labels))

    # Optimizer.
    # Find minimun of this loss using gradient descent
    optimizer = tf.train.GradientDescentOptimizer(0.5).minimize(loss)

    # Predictions for reporting accuracy figures.
    train_prediction = tf.nn.softmax(logits)

    valid_hidden = tf.nn.relu(tf.matmul(tf_valid_dataset, hidden_weights) + hidden_biases)
    valid_prediction = tf.nn.softmax(
            tf.matmul(valid_hidden, weights) + biases)

    test_hidden = tf.nn.relu(tf.matmul(tf_test_dataset, hidden_weights) + hidden_biases)
    test_prediction = tf.nn.softmax(
            tf.matmul(test_hidden, weights) + biases)


def accuracy(predictions, labels):
    return (100.0 * np.sum(np.argmax(predictions, 1) == np.argmax(labels, 1))
            / predictions.shape[0])

# do the actual computation
num_steps = 3001
with tf.Session(graph=graph) as session:
    # Initialize variables defined in graph once.
    tf.initialize_all_variables().run()
    print 'Initialized'

    for step in xrange(num_steps):

        # Pick an offset within the training data.
        offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
        batch_data = train_dataset[offset:(offset + batch_size), :]
        batch_labels = train_labels[offset:(offset + batch_size), :]

        # Create a diction telling the session wher to feed the minibatch.
        feed_dict = {
                tf_train_dataset: batch_data,
                tf_train_labels: batch_labels
        };

        _, l, predictions = session.run(
            [optimizer, loss, train_prediction], feed_dict=feed_dict)

        # reporting
        if (step % 500 == 0):
            print 'Minibatch loss at step', step, ':', l
            print 'Minibatch accuracy: %.1f%%' % accuracy(predictions,
                    batch_labels)
            print 'Validation accuracy: %.1f%%' % accuracy(
                    valid_prediction.eval(), valid_labels)

    print 'Test accuracy: %.1f%%' % accuracy(
            test_prediction.eval(), test_labels)


# real    0m8.149s
# user    0m6.844s
# sys     0m1.796s
# Test accuracy: 88.7%

