import cPickle as pickle
import numpy as np
import tensorflow as tf

image_size = 28
num_labels = 10

pickle_file = '../../1-machine-learning-to-deep-learning/1_notmnist/notMNIST.pickle'

def reformat(dataset, labels):
    """
    Reformat into a shape that's more adapted to the models we're going to
    train:

    - data as a flat matrix
    - labels as float 1-hot encoding
    """

    dataset = dataset.reshape(-1, image_size * image_size).astype(np.float32)
    # Map
    # 0 to [1.0, 0.0, 0.0, ...],
    # 1 to [0.0, 1.0, 0.0, ...], etc
    labels = (np.arange(num_labels) == labels[:, np.newaxis]).astype(np.float32)
    return dataset, labels

with open(pickle_file, 'rb') as f:
    save = pickle.load(f)
    train_dataset = save['train_dataset']
    train_labels = save['train_labels']
    valid_dataset = save['valid_dataset']
    valid_labels = save['valid_labels']
    test_dataset = save['test_dataset']
    test_labels = save['test_labels']

    # hint to gc
    del save

    print 'Training set', train_dataset.shape, train_labels.shape
    print 'Validation set', valid_dataset.shape, valid_labels.shape
    print 'Test set', test_dataset.shape, test_labels.shape

    train_dataset, train_labels = reformat(train_dataset, train_labels)
    valid_dataset, valid_labels = reformat(valid_dataset, valid_labels)
    test_dataset, test_labels = reformat(test_dataset, test_labels)

# prepare graph
train_subset = 10000
graph = tf.Graph()
with graph.as_default():
    # Load the training, validation and test data into constant that are
    # attached to the graph.
    tf_train_dataset = tf.constant(train_dataset[:train_subset, :])
    tf_train_labels = tf.constant(train_labels[:train_subset])
    tf_valid_dataset = tf.constant(valid_dataset)
    tf_test_dataset = tf.constant(test_dataset)

    # Weights are initialized using random values following a truncated normal
    # distribution.
    weights = tf.Variable(tf.truncated_normal([image_size * image_size, num_labels]))

    # Biases are initialized to zero.
    biases = tf.Variable(tf.zeros([num_labels]))

    # We multiply the inputs with the weight matrix, and add biases.
    logits = tf.matmul(tf_train_dataset, weights) + biases

    # We compute the softmax and cross-entropy and take average of this
    # cross-entropy across all training examples: that's our loss.
    loss = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(logits, tf_train_labels))

    # Optimizer.
    # Find minimun of this loss using gradient descent
    optimizer = tf.train.GradientDescentOptimizer(0.5).minimize(loss)

    # Predictions for reporting accuracy figures.
    train_prediction = tf.nn.softmax(logits)
    valid_prediction = tf.nn.softmax(
            tf.matmul(tf_valid_dataset, weights) + biases)
    test_prediction = tf.nn.softmax(
            tf.matmul(tf_test_dataset, weights) + biases)


num_steps = 801

def accuracy(predictions, labels):
    return (100.0 * np.sum(np.argmax(predictions, 1) == np.argmax(labels, 1))
            / predictions.shape[0])

# do the actual computation
with tf.Session(graph=graph) as session:
    # Initialize variables defined in graph once.
    tf.initialize_all_variables().run()
    print 'Initialized'
    for step in xrange(num_steps):
        _, l, predictions = session.run([optimizer, loss, train_prediction])

        # reporting
        if (step % 100 == 0):
            print 'Loss at step', step, ':', l
            print 'Training accuracy: %.1f%%' % accuracy(
                    predictions, train_labels[:train_subset, :])

            # Calling .eval() on valid_prediction is basically like calling run(),
            # but just to get that one numpy array. Note that it recomputes all its
            # graph dependencies.
            print 'Validation accuracy: %.1f%%' % accuracy(
                valid_prediction.eval(), valid_labels)

    print 'Test accuracy: %.1f%%' % accuracy(
            test_prediction.eval(), test_labels)

# Test accuracy: 82.0%
# real  0m5.035s
# user  0m4.124s
# sys 0m1.448s

